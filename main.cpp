#include <iostream>
#include <vector> 
using namespace std;

struct Knoten {
    int wert;
    Knoten* next;
};

Knoten* anker;
int anzahl = 0;

void initialisieren(int anz) {
    anzahl = anz;
    Knoten* knoten;
    Knoten* merkeZeiger; // zum zwischenspeichern der Adresse

    knoten = new Knoten(); // finde neuen Speicherblock im Heap
    knoten->next = nullptr; // letzter Knoten auf Nullzeiger

    for (int k = anz - 1; k >= 0; k--) {
        knoten->wert = 5 * (k + 3);
        merkeZeiger = knoten;
        knoten = new Knoten();
        knoten->next = merkeZeiger;
    }
    anker = knoten;
}

void zeigeAlles() {
    Knoten* knoten;
    knoten = anker;
    cout << "Anker";
    for (int k = 0; k <= anzahl - 1; k++) {
        knoten = knoten->next;
        cout << " -> " << knoten->wert;
    }
    cout << " -> Null" << endl;
}

void zeige(int nr) {
    Knoten* knoten;
    knoten = anker;
    for (int k = 0; k <= nr; k++)
        knoten = knoten->next;
    cout << nr << "-ter Eintrag: " << knoten->wert << endl;
}

void change(int nr, int neuerWert) {
    Knoten* knoten;
    knoten = anker;
    for (int k = 0; k <= nr; k++)
        knoten = knoten->next;
    knoten->wert = neuerWert;
    cout << nr << "-ter Eintrag neuer Wert: " << neuerWert << endl;
}

void loesche(int nr) {
    Knoten* knoten;
    knoten = anker;
    Knoten* merkeZeiger;
    for (int k = 0; k <= nr - 1; k++)
        knoten = knoten->next;
    merkeZeiger = knoten; // merke zu ändernde Adresse
    knoten = knoten->next;
    merkeZeiger->next = knoten->next;
    anzahl--;
    cout << nr << "-ter Eintrag geloescht." << endl;
}

void einfuegen(int nr, int wertNeu) {
    Knoten* knoten;
    knoten = anker;
    for (int k = 0; k <= nr; k++) {
        knoten = knoten->next;
    }
    Knoten* neu = new Knoten();
    Knoten* merkeKnoten = knoten->next;
    knoten->next = neu;
    neu->next = merkeKnoten;
    neu->wert = wertNeu;
    anzahl++;
    cout << "Nach " << nr << "-tem Knoten wurde eingefuegt: ";
    cout << "Knoten mit Wert " << wertNeu << endl;
}

int main() {
    //cout << "Zählung der Knoten beginnt mit 0!" << endl;
    cout << "Verkettete Liste" << endl;
    cout << "Wie viele Eintraege soll die Liste haben?" << endl;
    cout << "Bitte geben Sie eine Zahl zwischen 3 und 10 ein, dann Enter druecken!" << endl;
    int eingabe;
    cin >> eingabe;
    initialisieren(eingabe); // erstelle Liste mit 7 Einträgen
    cout << endl;
    zeigeAlles();
    cout << endl;

    bool weiter = true;
    int wert;
    int nummer;

    while (weiter == true) {
        cout << endl << "Bitte geben Sie eine 1, 2 oder 3 ein und dann Enter:" << endl;
        cout << "1  Knoten hinzufuegen" << endl;
        cout << "2  Knoten loeschen" << endl;
        cout << "3  Programm-Ende" << endl;
        cin >> eingabe;
        if(eingabe == 1) {
            cout << "Geben Sie eine Knotennummer ein, an der eingefuegt werden" << endl;
            cout << "soll und druecken Sie Enter. Z.B.: 2" << endl;
            cin >> nummer;
            cout << "Welchen Wert wollen Sie dort einfuegen?" << endl;
            cout << "Geben Sie eine ganze Zahl ein und druecken Sie Enter. Z.B.: 27" << endl;
            cin >> wert;
            einfuegen(nummer, wert);
            cout << endl;
        }
        if (eingabe == 2) {
            cout << "Geben Sie eine Knotennummer ein, deren Knoten geloescht" << endl;
            cout << "werden soll und druecken Sie Enter. Z.B.: 3" << endl;
            cin >> nummer;
            loesche(nummer);
            cout << endl;
        }
        if(eingabe == 3)
            weiter = false;
        if(eingabe != 3) zeigeAlles();
    }
    cout << endl << "- Programm-Ende -" << endl;

    return 0;
}